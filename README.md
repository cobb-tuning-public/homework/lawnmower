# Lawnmower
This repo contains a hex file compiled for X86 code CED. This code is a sample code of an single cylinder ECU.

## Deliverables
At a minimum, you should:
* Privately clone this repository and commit your work there
* Document the addresses of tables, maps, other important variables, undocumented protocols and or parameters
* Describe the basic control strategy of the ECU


## Known Knowns
It is a single cylinder engine from a lawnmower
### Outputs:
* Spark control
* Fuel control
### Inputs:
* Ignition switch
* O2 Sensor
* Throttle Position Sensor
* Mass Air Flow
* Manifold Air Pressure
* Engine Temperature
* Air Temperature
### Comunication Protocol
Example TX/RX exchange with the ECU

TX
```0x22 0x02 0x00 0x42```

RX
```0x62 0x01 0xa5 0xE8```

Fortunately we have a good communications person and they provided this breakdown:

* 0x22 <======= Mode 22 Read by ID
* 0x02 <======= MSB of ID
* 0x00 <======= LSB of ID
* 0x42 <======= Checksum

So this means the testing system is requesting via Mode 22 ID `0x200` and gets are response of:

* 0x62 <======= Mode 62 Acknowledge of Read by ID
* 0x01 <======= MSB of Data
* 0xa5 <======= LSB of  Data
* 0xE8 <======= Checksum

Which is the Mode 22 response with data of `0x1A5`

What this means is that somewhere in the code is a function or task, that translates the `0x200` to get the data `0x1A5`.

Thanks to that same great communications person they gave you this information:

    0x200, Air Temperature
    0x202, Engine Temperature
    0x300, Mass Air Flow
    0x301, Manifold Air Pressure
    0x500, O2 Sensor
    0x600, Throttle Position Sensor
    0x659, Engine Load
    0x700, Battery Voltage
    0x720, Engine Speed
    0x734, Spark Advance
    0x724, Fuel Pulsewidth

Now during their research they also found that if you sent a read by data identifier for other parameters that did not exist you would get a response of:

* 0x7F <========== Error
* 0x22 <========== Read by ID
* 0x31 <========== Request Out of range error

They also noted that some of the variables returned different lengths of data (i.e. not just two bytes).

## Resources
https://en.wikibooks.org/wiki/X86_Disassembly

https://en.wikipedia.org/wiki/Unified_Diagnostic_Services

The calling convention is CDECL
https://en.wikipedia.org/wiki/X86_calling_conventions

### Dissasemblers

https://hex-rays.com/ida-free/

https://binary.ninja/free/

https://ghidra-sre.org/


